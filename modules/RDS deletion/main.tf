###we need to import the exsiting RDS details
### >terraform import aws_db_instance.<identifier> <database id>
###/Sample/:  >terraform import aws_db_instance.mysql database-1
#
data "aws_db_instance" "vpc" {
  db_instance_identifier = var.db_name
}

resource "aws_db_instance" "mysql" {
  identifier = data.aws_db_instance.vpc.db_instance_identifier
  instance_class = data.aws_db_instance.vpc.db_instance_class
  allocated_storage = data.aws_db_instance.vpc.allocated_storage
# vpc_security_group_ids = data.aws_db_instance.vpc.vpc_security_groups
  engine = data.aws_db_instance.vpc.engine
  apply_immediately = true
  skip_final_snapshot = false
#  spcify time for identifier and give a tag to get idetified that it was done by terraform
  final_snapshot_identifier = "${var.db_name}-Caution"
  deletion_protection = false
  lifecycle {

    ignore_changes = [max_allocated_storage,tags,copy_tags_to_snapshot,monitoring_interval,instance_class,password]
  }
  depends_on = [null_resource.tag-vpc]
}
resource "null_resource" "tag-vpc" {
  provisioner "local-exec" {
    command = "terraform import aws_db_instance.mysql terraform-20211211140007317500000001 "
  }
  lifecycle {
    ignore_changes = [id]
  }
}

