data "aws_instance" "foo" {
  instance_id = "i-064228df5df887789"

  filter {
    name   = "image-id"
    values = ["ami-052cef05d01020f1d"]
  }

  filter {
    name   = "tag:Name"
    values = ["terraform"]
  }
}

resource "aws_instance" "patch" {
  ami           = data.aws_instance.foo.ami
  instance_type = data.aws_instance.foo.instance_type
  tags = {
    name        = "DevTaxilla"
    Patch-Group = "PlatformQA"
  }
}

