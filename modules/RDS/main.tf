data "aws_db_snapshot" "db_snapshot" {
  #most_recent = true
  db_instance_identifier = "RDS"
}

resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_subnet" "private-subnet1" {
  vpc_id = "${aws_vpc.main.id}"
  cidr_block = "10.0.2.0/24"
  availability_zone = "${var.availability_zone}"
}

resource "aws_subnet" "private-subnet2" {
  vpc_id = "${aws_vpc.main.id}"
  cidr_block = "10.0.3.0/24"
  availability_zone = "${var.availability_zone}"
}

resource "aws_db_subnet_group" "db-subnet" {
  subnet_ids = ["${aws_subnet.private-subnet1.id}", "${aws_subnet.private-subnet2.id}"]
  tags = {
    Name = "RDS creation"
  }
}

resource "aws_db_instance" "db_test" {
  instance_class = "db.t2.small"
  identifier = "newtestdb"
  username = "test"
  password = "Test@12345"
  publicly_accessible = false
  db_subnet_group_name = "${aws_db_subnet_group.db-subnet.name}"
  snapshot_identifier = "${data.aws_db_snapshot.db_snapshot.id}"
  vpc_security_group_ids = ["sg-00h62b79"]
  skip_final_snapshot = true
}

