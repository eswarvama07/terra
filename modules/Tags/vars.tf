variable "dept_name" {
  default = "DevOps"
}
variable "app_env" {
  default = "Dev"
}

variable "default_tags" {
  type = map
  default = {
    Patch-Group : "DevTaxilla"
  }
}

variable "aws_instance_id" {
  default = "i-0144ce9aefde6ff30"
}


variable "aws_instance_type" {
  default = "t2.micro"
}

variable "aws_vpc_id" {
  default = "vpc-0682b7847a9a37c7c"
}


variable "aws_subnet_id" {
  default = "subnet-05d38afac69b4e1bd"
}

variable "aws_sg" {
  default = "sg-05d27556e495232f9"
}

variable "keyfile" {
  default = "terraform"
}
#variable "instances" {
#  type = map(object({
#    host_id = ["i-0144ce9aefde6ff30","i-0653e71369d7b5ae7"]
#  }))
#}

locals {
  instance_ids = toset([
  "i-0653e71369d7b5ae7",
  "i-0d92ee33dc51a1696"
  ])
}

variable "tag_value" {
  type = string
}
variable "tag_name"{
  type = string
}

variable "resource_id" {
  type = string
}