
resource "aws_db_subnet_group" "db-subnet" {
  subnet_ids = var.aws_subnet_id
  tags = {
    Name = var.subnet_group_name
  }
}

resource "aws_db_instance" "mysql" {
  instance_class         = var.db_instance_type
  name                   = var.db_identifier
  allocated_storage      = var.allocated_storage
  max_allocated_storage  = var.max_storage
  username               = var.db_username
  password               = var.db_password
  engine                 = var.engine
  engine_version         = var.engine_version
  multi_az               = var.multi_az
  storage_type           = var.storage_type
  availability_zone      = var.availabiltiy_zone
  backup_retention_period = var.backup_retention_period
  backup_window           = var.backup_window
  copy_tags_to_snapshot   = var.copy_tags_to_snapshot
#  iam_database_authentication_enabled = var.iam_database_authentication_enabled
  auto_minor_version_upgrade = var.auto_minor_version_upgrade
  deletion_protection    = var.deletion_protection
#  maintenance_window     = var.maintenance_window

  vpc_security_group_ids = var.vpc_security_group_ids
#  replicate_source_db    = var.replicate_source_db
  port                   = var.port
  publicly_accessible    = var.public_acess
  db_subnet_group_name   = "${aws_db_subnet_group.db-subnet.name}"
  skip_final_snapshot    = var.skip_final_snapshot
  final_snapshot_identifier = "${var.db_identifier}-final-snapshot"

  tags = {
    name = "mysql RDS"
  }
}