variable "aws_subnet_id" {
  type = list
}
variable "vpc_security_group_ids" {
  type = list
}
variable "subnet_group_name"{
  type = string
}

variable "db_instance_type" {
  type = string
}
variable "db_identifier" {
  type  = string
}
variable "allocated_storage" {
  type = number
}
variable "db_username" {
  type = string
  sensitive = true
}
variable "db_password" {
  type = string
  sensitive = true
}
variable "engine" {
  type  = string
}
variable "engine_version" {
  type  = string
}
variable "port" {
  type = string
}
variable "multi_az" {
  type = bool
}
variable "storage_type" {
  type = string
}
variable "availabiltiy_zone" {
  type = string
}
variable "backup_retention_period" {
  type = number
}
#variable "create_with_snapshot" {
#  type  = string
#}
variable "backup_window"{
  type  = string
}
variable "copy_tags_to_snapshot" {
  type = bool
}
variable "iam_database_authentication_enabled" {
  type = bool
}
variable "auto_minor_version_upgrade" {
  type = bool
}
variable "deletion_protection" {
  type = bool
}
#variable "maintenance_window" {
#  type = string
#}
#variable "replicate_source_db" {
#  type = bool
#}
#variable "final_snapshot_identifier" {
#  type =string
#}
variable "public_acess" {
  type = bool
}
variable "skip_final_snapshot" {
  type = bool
}

variable "max_storage"{
  type = number
}
variable "snapshot_name" {
  type = string
}
