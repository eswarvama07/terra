variable "aws_instance_type" {
  default = "t2.micro"
}

variable "aws_vpc_id" {
  default = "vpc-0682b7847a9a37c7c"
}


variable "aws_subnet_id" {
  default = "subnet-05d38afac69b4e1bd"
}
variable "aws_sg" {
  default = "sg-0201d148ffbc9108a"
}

variable "keyfile" {
  default = "terraform"
}
