#//*
#
#resource "null_resource" "tag-vpc" {
#  provisioner "local-exec" {
#    command = "terraform import aws_db_instance.mysql terraform-20211211140007317500000001"
#  }
#}
#  lifecycle {
#    ignore_changes = [id]
#  }
#}
data "aws_db_instance" "type" {
  db_instance_identifier = var.db_id
}
#  filter {
#      name   = "tag:Name"
#      values = [var.db_id]
#    }
#  filter {
#      name = "vpc"
#      values = var.vpc_id
#    }
#  filter {
#      name = "Endpoint"
#      values = var.rds_endpoint
#
#    }


#
#
#
#
#resource "aws_db_instance" "mysql" {
#  instance_class = var.instance_type
#  allocated_storage = data.aws_db_instance.type.allocated_storage
#  identifier             = data.aws_db_instance.type.id
#  username               = data.aws_db_instance.type.master_username
#  vpc_security_group_ids = data.aws_db_instance.type.vpc_security_groups
#  engine                 = "mysql"
#  port                   = "3306"
#  storage_encrypted = data.aws_db_instance.type.storage_encrypted
#  lifecycle {
#ignore_changes = all
#}
#}
#
#*//
resource "aws_db_instance" "mysql" {

  instance_class = var.instance_type
  allocated_storage = data.aws_db_instance.type.allocated_storage
  apply_immediately = true
  engine = data.aws_db_instance.type.engine
#  deletion_protection = true
  copy_tags_to_snapshot = true
  monitoring_interval = data.aws_db_instance.type.monitoring_interval
  lifecycle {
    prevent_destroy = true
    create_before_destroy = true
    ignore_changes = [tags,max_allocated_storage,skip_final_snapshot]
  }


}



